var moduleName = require('./module');

var demoModule = require('./' + moduleName + '.js');

if (demoModule) {
    demoModule.sayHelloTo('Coding!');
} else {
    console.log('Module ' + moduleName + ' not exists!');
}