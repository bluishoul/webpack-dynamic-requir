# Run

1. run global webpack on watch mode

    ```
    webpack --config webpack.config.js --watch  --progress
    ```

2. open `index.html` 

3. change `src/module.js` exports into `hello`

4. refresh `index.html` and open broswer Console Pane

# Reference

https://webpack.github.io/docs/context.html