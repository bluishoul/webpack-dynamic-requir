/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	var moduleName = __webpack_require__(2);

	var demoModule = __webpack_require__(3)("./" + moduleName + '.js');

	if (demoModule) {
	    demoModule.sayHelloTo('Coding!');
	} else {
	    console.log('Module ' + moduleName + ' not exists!');
	}

/***/ },
/* 2 */
/***/ function(module, exports) {

	module.exports = 'say';


/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	var map = {
		"./entry.js": 1,
		"./hello.js": 4,
		"./module.js": 2,
		"./say.js": 5
	};
	function webpackContext(req) {
		return __webpack_require__(webpackContextResolve(req));
	};
	function webpackContextResolve(req) {
		return map[req] || (function() { throw new Error("Cannot find module '" + req + "'.") }());
	};
	webpackContext.keys = function webpackContextKeys() {
		return Object.keys(map);
	};
	webpackContext.resolve = webpackContextResolve;
	module.exports = webpackContext;
	webpackContext.id = 3;


/***/ },
/* 4 */
/***/ function(module, exports) {

	module.exports = {
	    sayHelloTo: function (name) {
	        console.log('!!Hello ' + name);
	    },
	};

/***/ },
/* 5 */
/***/ function(module, exports) {

	module.exports = {
	    sayHelloTo: function (name) {
	        alert('Say Hello ' + name);
	    },
	};

/***/ }
/******/ ]);